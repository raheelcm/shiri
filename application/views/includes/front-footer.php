</div>
<script type="text/javascript" src="<?= $assets; ?>js/jquery.js"></script>
<script type="text/javascript" src="<?= $assets; ?>js/bootstrap.min.js"></script>
<script type="text/javascript" src="<?= $assets; ?>js/owl.carousel.min.js"></script>

<script>
    $(document).ready(function() {
      $('.owl-carousel').owlCarousel({
        loop: true,
        responsiveClass: true,
        responsive: {
          0: {
            items: 1,
            nav: true
          },
          600: {
            items: 1,
            nav: false
          },
          1000: {
            items: 1,
            nav: true,
            loop: false
          }
        }
      })
    })
  </script>
</body>
</html>