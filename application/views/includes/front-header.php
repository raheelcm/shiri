<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Shireen al Mutawa</title>
    <link rel="stylesheet" type="text/css" href="<?= $assets; ?>css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="<?= $assets; ?>css/bootstrap.min.css">
    <link rel="stylesheet" href="<?= $assets; ?>css/owl.carousel.min.css">
    <link rel="stylesheet" type="text/css" href="<?= $assets; ?>css/owl.theme.default.min.css">
    <link rel="stylesheet" type="text/css" href="<?= $assets; ?>css/style.css">
</head>
<body>
<div id="wrapper">
    <div id="logo" class="mainLogo">
        <a href="index.html"><img src="<?= $assets; ?>images/logo.png"></a>
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-menu"><i class="fa fa-bars"></i></button>
    </div>
    
    <div class="sidebar">
        <div id="logo">
            <a href="index.html"><img src="<?= $assets; ?>images/logo.png"></a>
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-menu"><i class="fa fa-times"></i></button>
        </div>
        <ul class="menu">
            <li class="active"><a href="index.html">HOME</a></li>
            <li><a href="lessons.html">COURSES</a></li>
            <li><a href="aboutUs.html">ABOUT US</a></li>
            <li><a href="#">MARKETPLACE</a></li>
            <li><a href="sign-up.html">LOGIN/SIGNUP</a></li>
        </ul>
        <div id="logo" class="lightlogo">
            <a href="index"><img src="<?= $assets; ?>images/pic1.png"></a>
        </div>
        <ul class="languages">
            <li><a href="#">AR</a></li>
            <li class="active"><a href="#">EN</a></li>
        </ul>
        <ul class="social">
            <li><a href="#"><i class="fa fa-facebook-f"></i></a></li>
            <li><a href="#"><i class="fa fa-instagram"></i></a></li>
            <li>Shireenalmutawa</li>
        </ul>
    </div>