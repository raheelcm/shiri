<!-- BEGIN: Main Menu-->
    <div class="main-menu menu-fixed menu-light menu-accordion menu-shadow" data-scroll-to-active="true">
        <div class="navbar-header">
            <ul class="nav navbar-nav flex-row">
                <li class="nav-item mr-auto"><a class="navbar-brand" href="../../../html/ltr/vertical-menu-template/index.html">
                    <?php
                $user = $this->session->userdata('knet_login');
        if($user->roleID == 2)
        {
            ?>
            <img src="http://strokedev.com/coupons/assets/app-assets/images/portrait/small/cashier.png" style="width: 78%;margin: 13px auto;" />
                        
                        <?php
        }
        else
        {
            ?>
            <img src="http://strokedev.com/coupons/assets/app-assets/images/portrait/small/IMG-20200620-WA0008.png" style="width: 78%;margin: 13px auto;" />
            <?php
        }
                        ?>
                    </a></li>
            </ul>
        </div>
        <div class="shadow-bottom"></div>
        <div class="main-menu-content">
            <ul class="navigation navigation-main" id="main-menu-navigation" data-menu="menu-navigation">
                <li class=" nav-item"><a href="<?= base_url('admin/admin'); ?>"><i class="feather icon-home"></i><span class="menu-title" data-i18n="Dashboard">Dashboard</span><span class="badge badge badge-warning badge-pill float-right mr-2 d-none">2</span></a>
                </li>
                <?php
                $user = $this->session->userdata('knet_login');
        if($user->roleID == 1)
        {
            ?>
            
                <li class=" nav-item"><a href="#"><i class="feather icon-users"></i><span class="menu-title" data-i18n="Ecommerce">Salesman</span></a>
                    <ul class="menu-content">
                        <li><a href="<?= base_url('admin/artist/create');?>"><i class="feather icon-circle"></i><span class="menu-item" data-i18n="Shop">Add Salesman</span></a>
                        </li>
                        <li><a href="<?= base_url('admin/artist/all');?>"><i class="feather icon-circle"></i><span class="menu-item" data-i18n="Details">Manage Salesmans</span></a>
                        </li>
                    </ul>
                </li>
                <li class=" nav-item"><a href="#"><i class="feather icon-users"></i><span class="menu-title" data-i18n="Ecommerce">Cashier</span></a>
                    <ul class="menu-content">
                        <li><a href="<?= base_url('admin/cashier/create');?>"><i class="feather icon-circle"></i><span class="menu-item" data-i18n="Shop">Add Cashier</span></a>
                        </li>
                        <li><a href="<?= base_url('admin/cashier/all');?>"><i class="feather icon-circle"></i><span class="menu-item" data-i18n="Details">Manage Cashiers</span></a>
                        </li>
                    </ul>
                </li>
                <li class=" nav-item"><a href="#"><i class="feather icon-users"></i><span class="menu-title" data-i18n="Ecommerce">Promotions</span></a>
                    <ul class="menu-content">
                        <li><a href="<?= base_url('admin/genres/create');?>"><i class="feather icon-circle"></i><span class="menu-item" data-i18n="Shop">Add Promotion</span></a>
                        </li>
                        <li><a href="<?= base_url('admin/genres/all');?>"><i class="feather icon-circle"></i><span class="menu-item" data-i18n="Details">Manage Promotions</span></a>
                        </li>
                    </ul>
                </li>
                <?php
        }
        if($user->roleID == 3)
        {
            ?>
            
                <li class=" nav-item"><a href="<?= base_url('admin/admin/clients'); ?>"><i class="feather icon-home"></i><span class="menu-title" data-i18n="Dashboard">Clients</span><span class="badge badge badge-warning badge-pill float-right mr-2 d-none">2</span></a>
                </li>
            <li class=" nav-item"><a href="#"><i class="feather icon-users"></i><span class="menu-title" data-i18n="Ecommerce">Reseller</span></a>
                    <ul class="menu-content">
                        <li><a href="<?= base_url('admin/reseller/create');?>"><i class="feather icon-circle"></i><span class="menu-item" data-i18n="Shop">Add Reseller</span></a>
                        </li>
                        <li><a href="<?= base_url('admin/reseller/all');?>"><i class="feather icon-circle"></i><span class="menu-item" data-i18n="Details">Manage Resellers</span></a>
                        </li>
                    </ul>
                </li>
                <li class=" nav-item"><a href="#"><i class="feather icon-users"></i><span class="menu-title" data-i18n="Ecommerce">Coupon</span></a>
                    <ul class="menu-content">
                        <li><a href="<?= base_url('admin/coupon/create');?>"><i class="feather icon-circle"></i><span class="menu-item" data-i18n="Shop">Add Coupon</span></a>
                        </li>
                        <li><a href="<?= base_url('admin/coupon/all');?>"><i class="feather icon-circle"></i><span class="menu-item" data-i18n="Details">Manage Coupon</span></a>
                        </li>
                    </ul>
                </li>
                <!--
                <li class=" nav-item"><a href="#"><i class="feather icon-users"></i><span class="menu-title" data-i18n="Ecommerce">Power Card</span></a>
                    <ul class="menu-content">
                        <li><a href="<?= base_url('admin/genres/create');?>"><i class="feather icon-circle"></i><span class="menu-item" data-i18n="Shop">Add Power Card</span></a>
                        </li>
                        <li><a href="<?= base_url('admin/genres/all');?>"><i class="feather icon-circle"></i><span class="menu-item" data-i18n="Details">Manage Power Card</span></a>
                        </li>
                    </ul>
                </li>
                -->
                <?php
        }
        elseif($user->roleID == 4)
        {
            ?>
            
                <li class=" nav-item"><a href="<?= base_url('admin/admin/clients'); ?>"><i class="feather icon-home"></i><span class="menu-title" data-i18n="Dashboard">Clients</span><span class="badge badge badge-warning badge-pill float-right mr-2 d-none">2</span></a>
                </li>
            <li class=" nav-item"><a href="<?= base_url('admin/coupon/all');?>"><i class="feather icon-home"></i><span class="menu-title" data-i18n="Dashboard">My coupon</span><span class="badge badge badge-warning badge-pill float-right mr-2 d-none">2</span></a>
                </li>
            <?php
        }
                ?>
            </ul>
        </div>
    </div>